import React from "react";

interface DefaultLayoutProps {
    age: number;
}

interface DefaultLayoutState {
    age: number;
}

export class DefaultLayout extends React.Component<DefaultLayoutProps, DefaultLayoutState> {
    render() {
        Count({count: 3})
        return null;
    }
}

interface CountProps {
    count: number;
}

const Count: React.FunctionComponent<CountProps> = (props) => {
    return <h1>{props.count}</h1>;
}

