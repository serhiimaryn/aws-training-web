import React from "react";

import * as Types from "../../types/index";

export default class DogDetails extends React.PureComponent<Types.DogDetailsProps> {
    render() {
        if (!this.props.dog) {
            return (
                <span>Please select a dog from the list</span>
            );
        }

        return (
            <div className="row dog-names-list__container">
                <div className="col-12">                    
                    <img 
                        style={{ objectFit: "contain", maxWidth: 600, maxHeight: 600 }} 
                        src={this.props.dog.imageUrl} 
                        alt={this.props.dog.name} 
                    />
                </div>
            </div>
        );
    }
}