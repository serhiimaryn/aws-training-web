import React from "react";

import * as Types from "../../types/index";

const DogListItem: React.FC<Types.DogListItemProps> = ({ dog, selectedDogId, onDogItemSelected }) => {
    return (
        <li 
            className={`dog-list-item ${dog.id === selectedDogId ? "selected" : ""}`}
            onClick={() => onDogItemSelected(dog.id)}
        >
            {dog.name}
        </li>
    );
};


export default class DogNamesList extends React.PureComponent<Types.DogNamesListComponentProps> {
    render() {
        return (
            <div className="row dog-names-list__container">
                <div className="col-12">
                    <ul>
                        {this.props.dogs.map((dog, index) => {
                            return (
                                <DogListItem
                                    key={index}
                                    dog={dog}
                                    selectedDogId={this.props.selectedDogId}
                                    onDogItemSelected={this.props.onDogItemSelected}
                                />
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}