export interface AppProps {
    isProd: boolean;
    authorName: string;    
    defaultCount?: number;
}

export interface AppState {
    dogs: Array<DogEntity>;
    selectedDogId?: string | null;
    isModalOpen: boolean;
    newDogName: string | undefined;
    newDogImage: any | undefined;
}

export interface DogEntity { 
    id: string;
    name: string;
    imageUrl: string;
}

export interface DogNamesListComponentProps {
    dogs: Array<DogEntity>;
    selectedDogId?: string | null;
    onDogItemSelected(id: string) : void;
}

export interface DogListItemProps {
    dog: DogEntity;
    selectedDogId?: string | null;
    onDogItemSelected(id: string) : void;
}

export interface DogDetailsProps {
    dog?: DogEntity;
}