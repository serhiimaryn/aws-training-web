import React from "react";
import Modal from 'react-modal';

import "./styles/App.scss";

import * as Types from "./types/index";
import Header from "./components/header/header.component";
import DogNamesList from "./components/dog-names-list/dog-names-list.component";
import DogDetails from "./components/dog-details/dog-details.component";

Modal.setAppElement('#root');

class App extends React.PureComponent<Types.AppProps, Types.AppState> {
  state: Types.AppState = {
    selectedDogId: "4ed95e21-a1f8-4b13-9789-2a62923daf9d",
    dogs: [
      {
        id: "4ed95e21-a1f8-4b13-9789-2a62923daf9d",
        name: "Charlie",
        imageUrl: "https://i.ytimg.com/vi/MPV2METPeJU/maxresdefault.jpg"
      },
      {
        id: "3cfb26cb-289d-43d5-ad70-f174f9cdb95e",
        name: "Max",
        imageUrl: "https://www.medicalnewstoday.com/content/images/articles/322/322868/golden-retriever-puppy.jpg"
      },
      {
        id: "e62828a3-1b5f-457d-8b6b-59882fc5b5ea",
        name: "Bella",
        imageUrl: "https://www.nationalgeographic.com/content/dam/animals/thumbs/rights-exempt/mammals/d/domestic-dog_thumb.jpg"
      },
      {
        id: "84669063-0e6f-4afe-8072-6a66071e236f",
        name: "Oscar",
        imageUrl: "https://www.guidedogs.org/wp-content/uploads/2019/11/website-donate-mobile.jpg"
      },
      {
        id: "906e0203-3f95-412e-83db-37d5696ff736",
        name: "Molly",
        imageUrl: "https://boygeniusreport.files.wordpress.com/2016/11/puppy-dog.jpg?quality=98&strip=all&w=782"
      }
    ],
    isModalOpen: true,
    newDogName: undefined,
    newDogImage: undefined
  };

  onDogItemSelected = (id: string) => {
    this.setState({
      selectedDogId: id
    });
  }

  openModal = () => {
    this.setState({
      isModalOpen: true,
    });
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      newDogName: undefined
    });
  }

  updateNewDogName = (name: string | undefined) => {
    this.setState({
      newDogName: name
    });
  }

  fileSelectedHandler = (event: any) => {
    this.setState({
      newDogImage: URL.createObjectURL(event.target.files[0])
    });
  }

  fileInput: HTMLInputElement | null | undefined;

  render() {
    return (
      <div className="app-container">
        {/* <aside>
          Menu here
        </aside> */}
        <main>
          <Header />
          <section className="app__content container">
            <div className="row">
              <div className="content__actions col-12 d-flex justify-content-end align-item-end">
                <button
                  className="ds__button"
                  onClick={this.openModal}
                >
                  Create
                </button>
              </div>
              <div className="col-12 col-md-6 col-lg-3">
                <DogNamesList
                  dogs={this.state.dogs}
                  selectedDogId={this.state.selectedDogId}
                  onDogItemSelected={this.onDogItemSelected}
                />
              </div>
              <div className="col-12 col-md-6 col-lg-9">
                <DogDetails dog={this.state.dogs.find(x => x.id === this.state.selectedDogId)} />
              </div>
            </div>

          </section>
        </main>
        <Modal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.closeModal}
          style={{
            content: {
              top: '50%',
              left: '50%',
              right: 'auto',
              bottom: 'auto',
              marginRight: '-50%',
              transform: 'translate(-50%, -50%)'
            }
          }}
        >
          <div className="modal__content-container">
            <h2>Create</h2>
            <div className="content__wrapper modal__dog_seeker d-flex flex-column">
              <div className="ds__field">
                <label className="ds__input__label">Dog name</label>
                <input
                  className="ds__input"
                  onChange={(e) => this.updateNewDogName(e.target.value)}
                  value={this.state.newDogName}
                  style={{ marginLeft: 10 }}
                />
              </div>

              <div className="ds__field">
                <label className="ds__input__label">Dog photo</label>
                <input
                  type="file"
                  accept="image/*"
                  hidden={true}
                  onChange={e => this.fileSelectedHandler(e)}
                  ref={fileInput => this.fileInput = fileInput}
                />
                <div>
                  <button
                    onClick={() => this.fileInput?.click()}
                    className="ds__button simple"
                    style={{ marginLeft: 10 }}
                  >
                    Pick a photo
                  </button>
                </div>
                <div style={{ minHeight: 200, marginTop: 20, marginLeft: 10 }} >
                  {this.state.newDogImage &&
                    <img src={this.state.newDogImage} alt="new-dog-img" style={{ maxHeight: 200, objectFit: "contain" }} />
                  }
                </div>
              </div>
            </div>
            <div className="content__actions">
              <button className="ds__button simple" onClick={this.closeModal}>close</button>
              <button
                className={"ds__button"}
                onClick={() => { }}
                disabled={!this.state.newDogName || this.state.newDogName?.length < 3 || !this.state.newDogImage}
              >
                create
              </button>

            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default App;
